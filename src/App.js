import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import * as actionCreator from './store/actions/action'
import logo from './logo.svg';

class Shower extends Component {

  style = {
    one: {
      border: '3px solid pink',
      height: '200px'
    }
  }
  render() {
    return (
      <div style={this.style.one}>
        <h1>Shower</h1>
      </div>
    )
  }
}


class Power extends Component {

  style = {
    one: {
      border: '3px solid pink',
      height: '200px'
    },
    two: {
      border: '5px solid orange'
    }
  }
  render() {
    return (
      <div style={this.style.two}>
        <h1>Power</h1>
      </div>
    )
  }
}


class App extends Component {
  state = {
    age: 21
  }
  render() {
    return (
      <div className="App">
        <div><span>A:</span><span>{this.props.age}</span></div>
        <button onClick={this.props.onAgeUp}>AgeUp</button>
        <button onClick={this.props.onAgeDown}>AgeDown</button>
        {this.props.loading && <img className="App-logo" src={logo} />}
        {/* {this.props.loading && <Shower />} */}
        {this.props.loading ? <Shower /> : <Power />}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    age: state.age,
    loading: state.loading
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onAgeUp: () => dispatch(actionCreator.ageUp(1)),
    onAgeDown: () => dispatch(actionCreator.ageDown(1))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
