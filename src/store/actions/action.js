




//imagin this is the api call
export const ageUpAsnc = (val) => {
    return { type: 'AGE_UP', value: val }
}

//and this is the one being called in the app directly
//this will call the aysnc function after 5 seconds. Imagin that the 
//aysnc does not come back with 200 then it would just die here 
//before ever getting to the reducer. 
//THIS IS THE MIDDLE WARE PART
//IF ITS GOOD THEN ON TO TO THE REDUCER THEN TO STORE!!

export const ageUp = (val) => {



    return dispatch => {

        dispatch(loading())
        //above is synchronous

        //below is aysnc
        setTimeout(() => {
            dispatch(ageUpAsnc(val));
        }, 5000)
    }
}


export const ageDown = (val) => {
    return { type: 'AGE_DOWN', value: val }
}



export const loading = () => {
    return { type: 'LOADING' }
}