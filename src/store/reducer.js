
const initialState = {
    age: 21,

}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'AGE_UP':
            return {
                ...state,
                age: state.age + action.value
            }
            break;
        case 'AGE_DOWN':
            return {
                ...state,
                age: state.age - action.value,
                loading: false
            }
            break;
        case 'LOADING':
            return {
                ...state,
                loading: true
            }
    }
    return state;


}


export default reducer;